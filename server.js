const express = require('express');
const app = express()
const router = express.Router()
// router.use(compression())
router.use(express.static(`web/public`))
// this would be needed if you use client routes/`matchPath` (see https://github.com/gatsbyjs/gatsby/blob/master/packages/gatsby/src/commands/serve.js for implementation of `clientOnlyPathsRouter`)
// router.use(clientOnlyPathsRouter(pages, { root }))
// serve 404 if nothing found
router.use((req, res, next) => {
  if (req.accepts(`html`)) {
    return res.status(404).sendFile(`404.html`, { root })
  }
  return next()
})
app.use(function(req, res, next) {
//   res.header(`Access-Control-Allow-Origin`, `http://${host}:${port}`)
  res.header(`Access-Control-Allow-Credentials`, true)
  res.header(
    `Access-Control-Allow-Headers`,
    `Origin, X-Requested-With, Content-Type, Accept`
  )
  next()
})
// use same pathPrefix as one you build site with
app.use('/', router)

const port = process.env.PORT || 3069;
app.listen(port);

console.log('App is listening on port ' + port);